+++
fragment = "contact"
disabled = false
date = "2021-12-27"
weight = 1100
#background = "light"
form_name = "defaultContact"

title = "Contact fragment"
subtitle  = "Contact us for project requests"

# PostURL can be used with backends such as mailout from caddy
post_url = "https://formspree.io/f/xoqrvvgb" #default: formspree.io
email = "mail@example.com"
button = "Send Button" # defaults to theme default
#netlify = false

# Optional google captcha
#[recaptcha]
# sitekey = "6Lcyis8dAAAAAJs74oFm654YrGUXYNNBAZHInUmA"

[message]
  success = "message send" # defaults to theme default
  error = "error sending message" # defaults to theme default

# Only defined fields are shown in contact form
[fields.name]
  text = "Your Name *"
  #error = "" # defaults to theme default

[fields.email]
  text = "Your Email *"
  #error = "" # defaults to theme default

#[fields.phone]
#  text = "Your Phone *"
#  #error = "" # defaults to theme default

[fields.message]
  text = "Your Message *"
  #error = "" # defaults to theme default

# Optional hidden form fields
# Fields "page" and "site" will be autofilled
[[fields.hidden]]
  name = "page"

[[fields.hidden]]
  name = "someID"
  value = "example.com"
+++
